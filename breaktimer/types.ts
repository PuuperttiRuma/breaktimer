
/**
 * type: string, Type of the event, eg. water, break, etc.
 * timestamp: Date; When the event was created
 */
export interface TimerEvent {
  id?: number;
  type: string; //TODO: Convert to Enum
  timestamp: Date;
}

declare module "express" {
  export interface Request {
    timerEvent?: TimerEvent;
  }
}