import express, { Request, Response, NextFunction } from "express";
import cors from "cors"
import { TimerEvent } from "../types.ts";

const server = express();

const port = 3000;

const timerEvents: TimerEvent[] = [];

let nextId = 1;
const idGenerator = () => nextId++;

// Middleware definitions

/**
 * Validates if the Request body has a valid TimerEvent in it
 * and adds the TimerEvent as a property to the Request.
 * req.timerEvent = {type, timestamp}
 */
const validateEvent = (req: Request, res: Response, next: NextFunction) => {
  const { type, timestamp } = req.body;
  if (typeof type !== "string") {
    return res
      .status(400)
      .send(`Event Type: Expected string, was ${typeof type}`);
  }
  const timestampDate = new Date(timestamp);
  if ( isNaN( timestampDate.getTime() ) ){
    res.status(400).send("Event timestamp not a valid ISOStringDate")
  }
    req.timerEvent = {
      type: type,
      timestamp: timestamp,
    };
  next();
};

// Middleware declarations
server.use(express.json());
server.use(express.urlencoded({ extended: false }));
server.use(cors());

server.get("/events", (req: Request, res: Response) => {
  res.send(timerEvents);
});

server.post("/events", validateEvent, (req: Request, res: Response) => {
  const { type, timestamp } = req.timerEvent as TimerEvent;
  timerEvents.push({
    id: idGenerator(),
    type: type,
    timestamp: timestamp,
  });
  res.status(201).send();
});

server.listen(port, () => {
  console.log(`Listening to port ${port}`);
});
