import "./App.css";
import WorkView from "./components/WorkView";
import { TimerEvent } from "../types.ts";
import { postEvent } from "./services.ts";

const events: TimerEvent[] = [];

function App() {
  const handleClick = (eventType: string) => {
    const newEvent = ({
      type: eventType,
      timestamp: new Date(),
    });
    events.push(newEvent);
    postEvent(newEvent);
  };

  return (
    <div>
      <WorkView handleClick={handleClick} />
    </div>
  );
}

export default App;
