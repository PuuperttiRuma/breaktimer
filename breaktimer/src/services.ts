import axios from "axios";
import { TimerEvent } from "../types";

const url = "http://localhost:3000"

export const postEvent = async (event: TimerEvent) => {
  try {
    const response = await axios.post(url + "/events", event);
    return response.data;
  } catch (error) {
    console.log(error);
  }
};
