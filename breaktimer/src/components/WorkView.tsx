import ActionButtons from "./ActionButtons";
import Clock from "./Clock";
//import Timer from "./Timer";

interface WorkViewProps {
  handleClick: (eventType: string) => void;
}

const WorkView = ({handleClick}: WorkViewProps) => {
  return <div>
    <Clock />
    {/* <Timer /> */}
    <ActionButtons handleClick={handleClick} />
  </div>
};

export default WorkView;