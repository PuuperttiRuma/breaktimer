interface ActionButtonsProps {
  handleClick: (eventType: string) => void;
}

const ActionButtons = ({handleClick}: ActionButtonsProps) => {
  return (
    <div className="button-container">
      <button onClick={() => handleClick("break")}>Break</button>
      <button onClick={() => handleClick("water")}>Water</button>
      <button onClick={() => handleClick("meditation")}>Meditate</button>
      <button onClick={() => handleClick("yoga")}>Yoga</button>
      <button onClick={() => handleClick("slump")}>Slump</button>
    </div>
  );
};

export default ActionButtons;
